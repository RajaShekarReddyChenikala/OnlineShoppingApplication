﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLayer
{
    public class BOLProductMaster
    {
        public string ProductName { get; set; }
        public bool Active { get; set; }
        public int SubCategoryId { get; set; }

        public int MainCategoryId { get; set; }
    }
}
