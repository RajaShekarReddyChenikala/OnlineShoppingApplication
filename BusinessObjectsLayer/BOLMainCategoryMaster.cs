﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLayer
{
    public class BOLMainCategoryMaster
    {
        public string MainCategoryName { get; set; }

        public bool Active { get; set; }
    }
}
