﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLayer
{
    public class BOLSubCategoryMaster
    {
        public string SubCategoryName { get; set; }
        public bool Active { get; set; }
        public int MainCategoryId { get; set; }
    }
}
