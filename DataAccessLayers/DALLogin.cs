﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BusinessObjectsLayer;

namespace DataAccessLayers
{
    public class DALLogin
    {
        public string CheckCredentials(BOLLogin bollogin)
        {
                DataSet ds = new DataSet();
                SQLConnection.OpenConnection();
                SqlCommand cmd = new SqlCommand("Check_Logins", SQLConnection.Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", bollogin.Email);
                cmd.Parameters.AddWithValue("@Password", bollogin.Password);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                var email = Convert.ToString(ds.Tables[0].Rows[0].ItemArray[0]);
                var password = Convert.ToString(ds.Tables[0].Rows[0].ItemArray[1]);
            if (bollogin.Email==email && bollogin.Password==password)
            {
                return "Matched";
            }
            else
            {
                return "Not Matched";
            }      
        }

    }
}
