﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BusinessObjectsLayer;

namespace DataAccessLayers
{
    public class DALSignup
    {
        public int PostSignupCredentials(BOLSignup bolsignup)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Post_Signups", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", bolsignup.UserName);
            cmd.Parameters.AddWithValue("@Email", bolsignup.Email);
            cmd.Parameters.AddWithValue("@Password",(bolsignup.Password));
            cmd.Parameters.AddWithValue("@ConfirmPassword", (bolsignup.ConfirmPassword));
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }

    }
    //public static class EncryptDecrypt
    //{
    //    public static string Encrypt(string password)
    //    {
    //        byte[] EncryptDataByte = new byte[password.Length];
    //        EncryptDataByte = Encoding.UTF8.GetBytes(password);
    //        string EncryptedData = Convert.ToBase64String(EncryptDataByte);
    //        return EncryptedData;
    //    }
    //    public static string Decrypt(string EncryptedData)
    //    {
    //        UTF8Encoding encoder = new UTF8Encoding();
    //        Decoder UTF8Decode = encoder.GetDecoder();
    //        byte[] DecodeByte = Convert.FromBase64String(EncryptedData);
    //        int CharCount = UTF8Decode.GetCharCount(DecodeByte, 0, DecodeByte.Length);
    //        char[] DecodeChar = new char[CharCount];
    //        UTF8Decode.GetChars(DecodeByte, 0, DecodeByte.Length, DecodeChar, 0);
    //        string DecryptedData = new string(DecodeChar);
    //        return DecryptedData;
    //    }
    //}
}
