﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccessLayers
{
    public static class SQLConnection
    {
        static SqlConnection con;
        public static SqlConnection Conn { get { return con; } }

        public static void OpenConnection()
        {
            string conStr = ConfigurationManager.ConnectionStrings["constr"].ToString();
            con = new SqlConnection(conStr);
            con.Open();
        }

        public static void CloseConnection()
        {
            con.Close();
        }
    }
}
