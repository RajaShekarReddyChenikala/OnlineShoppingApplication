﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BusinessObjectsLayer;

namespace DataAccessLayers
{
    public class DALMainCategoryMaster
    {
        public int PostMainCategoryDetails(BOLMainCategoryMaster bolmain)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Post_Main_Category_Master_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MainCategoryName", bolmain.MainCategoryName);
            cmd.Parameters.AddWithValue("@Active", bolmain.Active);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
        public DataTable GetAllMainCategoryDetails()
        {
            SQLConnection.OpenConnection();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("Get_All_Main_Category_Details", SQLConnection.Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;  
        }
        public int DeleteMainCategoryDetails(int id)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Delete_Main_Category_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MainCategoryId",id );
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
        public int UpdateMainCategoryDetails(int id, BOLMainCategoryMaster bolmain)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Update_Main_Category_Details",SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MainCategoryId", id);
            cmd.Parameters.AddWithValue("@MainCategoryName", bolmain.MainCategoryName);
            cmd.Parameters.AddWithValue("@Active", bolmain.Active);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
    }
}
