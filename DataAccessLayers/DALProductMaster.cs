﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLayer;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccessLayers
{
    public class DALProductMaster
    {
        public int PostProductMasterDetails(BOLProductMaster bolproduct)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Post_Product_Master_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProductName", bolproduct.ProductName);
            cmd.Parameters.AddWithValue("@Active", bolproduct.Active);
            cmd.Parameters.AddWithValue("@SubCategoryId", bolproduct.SubCategoryId);
            cmd.Parameters.AddWithValue("@MainCategoryId", bolproduct.MainCategoryId);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
        public DataTable GetSubCategoryNames(int id)
        {
            SQLConnection.OpenConnection();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("Get_SubCategory_Names", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MainCategoryId", id );
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public DataTable GetAllProductMasterDetails()
        {
            SQLConnection.OpenConnection();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("Get_All_Product_Master_Details", SQLConnection.Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public int DeleteProductMasterDetails(int id)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Delete_Product_Master_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProductId", id);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
    }
}
