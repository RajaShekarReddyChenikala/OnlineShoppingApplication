﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLayer;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccessLayers
{
    public class DALSubCategoryMaster
    {
        public int PostSubCategoryDetails(BOLSubCategoryMaster bolsub)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Post_Sub_Category_Master_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubCategoryName", bolsub.SubCategoryName);
            cmd.Parameters.AddWithValue("@Active", bolsub.Active);
            cmd.Parameters.AddWithValue("@MainCategoryId", bolsub.MainCategoryId);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
        public DataTable GetMainCategoryName()
        {
            SQLConnection.OpenConnection();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("select MainCategoryId,MainCategoryName from Main_Category_Master", SQLConnection.Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public DataTable GetAllSubCategoryDetails()
        {
            SQLConnection.OpenConnection();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("Get_All_Sub_Category_Details", SQLConnection.Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public int DeleteSubCategoryDetails(int id)
        {
            SQLConnection.OpenConnection();
            SqlCommand cmd = new SqlCommand("Delete_Sub_Category_Details", SQLConnection.Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubCategoryId", id);
            int i = cmd.ExecuteNonQuery();
            SQLConnection.CloseConnection();
            return i;
        }
    }
}
