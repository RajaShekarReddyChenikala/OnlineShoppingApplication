	
	CREATE DATABASE OnlineShopping

	USE OnlineShopping

	CREATE TABLE Signup(SignupId INT IDENTITY(1,1) PRIMARY KEY ,UserName VARCHAR(50),Email VARCHAR(50),Password VARCHAR(50),ConfirmPassword VARCHAR(50))

	CREATE TABLE Logins(LoginId INT IDENTITY(1,1) PRIMARY KEY ,UserName VARCHAR(50),Password VARCHAR(50),Active BIT,LastLogin DATETIME)

	CREATE TABLE User_Logs(LogId INT PRIMARY KEY, Login DATETIME,LoginId INT FOREIGN KEY REFERENCES Logins(LoginId))

	CREATE TABLE Main_Category_Master(MainCategoryId INT IDENTITY(1,1) PRIMARY KEY,MainCategoryName VARCHAR(50),Active BIT)

	CREATE TABLE Sub_Category_Master(SubCategoryId INT IDENTITY(1,1) PRIMARY KEY,SubCategoryName VARCHAR(50),Active BIT,MainCategoryId INT FOREIGN KEY REFERENCES Main_Category_Master(MainCategoryId))

	CREATE TABLE Product_Master(ProductId INT IDENTITY(1,1) PRIMARY KEY,ProductName VARCHAR(50),Active BIT,SubCategoryId INT FOREIGN KEY REFERENCES Sub_Category_Master(SubCategoryId),MainCategoryId INT FOREIGN KEY REFERENCES Main_Category_Master(MainCategoryId))

	--SIGNUP
	CREATE PROCEDURE Post_Signups(@UserName VARCHAR(50),@Email VARCHAR(50),@Password VARCHAR(50))
	AS BEGIN 
	INSERT INTO Signup VALUES(@UserName,@Email,@Password)
	END
	
	--Login
	CREATE PROCEDURE Post_Logins(@UserName VARCHAR(50),@Password VARCHAR(50),@Active BIT,@LastLogin DATETIME)
	AS BEGIN 
	INSERT INTO Logins VALUES(@UserName,@Password,@Active,@LastLogin)
	END

	--MAIN CATEGORY DETAILS

	CREATE PROCEDURE Get_All_Main_Category_Details
	AS BEGIN 
	SELECT * FROM Main_Category_Master
	END

	EXEC Get_All_Main_Category_Details


	CREATE PROCEDURE Post_Main_Category_Master_Details(@MainCategoryName VARCHAR(50),@Active BIT)
	AS BEGIN 
	INSERT INTO Main_Category_Master VALUES(@MainCategoryName,@Active)
	END

	CREATE PROCEDURE Delete_Main_Category_Details(@MainCategoryId INT)
	AS BEGIN 
	DELETE FROM Main_Category_Master WHERE MainCategoryId=@MainCategoryId
	END

	CREATE PROCEDURE Update_Main_Category_Details(@MainCategoryId INT,@MainCategoryName VARCHAR(50),@Active BIT)
	AS BEGIN 
	UPDATE Main_Category_Master SET MainCategoryName=@MainCategoryName,Active=@Active WHERE MainCategoryId=@MainCategoryId
	END


	--SUB CATEGORY DETAILS

	CREATE PROCEDURE Get_All_Sub_Category_Details
	AS BEGIN 
	SELECT * FROM Sub_Category_Master
	END

	CREATE PROCEDURE Post_Sub_Category_Master_Details(@SubCategoryName VARCHAR(50),@Active BIT,@MainCategoryId INT)
	AS BEGIN 
	INSERT INTO Sub_Category_Master VALUES(@SubCategoryName,@Active,@MainCategoryId)
	END

	CREATE PROCEDURE Delete_Sub_Category_Details(@SubCategoryId INT)
	AS BEGIN 
	DELETE FROM Sub_Category_Master WHERE SubCategoryId=@SubCategoryId
	END

	CREATE PROCEDURE Update_Sub_Category_Details(@SubCategoryId INT,@SubCategoryName VARCHAR(50),@Active BIT)
	AS BEGIN 
	UPDATE Sub_Category_Master SET SubCategoryName=@SubCategoryName,Active=@Active WHERE SubCategoryId=@SubCategoryId
	END

	--PRODUCT MASTER 

	CREATE PROCEDURE Get_All_Product_Master_Details
	AS BEGIN 
	SELECT * FROM Product_Master
	END

	CREATE PROCEDURE Post_Product_Master_Details(@ProductName VARCHAR(50),@Active BIT,@SubCategoryId INT,@MainCategoryId INT)
	AS BEGIN 
	INSERT INTO Product_Master VALUES(@ProductName,@Active,@SubCategoryId,@MainCategoryId)
	END

	CREATE PROCEDURE Delete_Product_Master_Details(@ProductId INT)
	AS BEGIN 
	DELETE FROM Product_Master WHERE ProductId=@ProductId
	END

	CREATE PROCEDURE Get_Login_Details
	AS BEGIN 
	SELECT UserName,Password FROM Logins
	END


	EXEC Get_Login_Details

	SELECT * FROM Logins

	TRUNCATE TABLE Logins

	SELECT * FROM Main_Category_Master

	SELECT * FROM Sub_Category_Master

	SELECT * FROM Product_Master

	--INNER JOIN
	SELECT MainCategoryName,SubCategoryName FROM Main_Category_Master INNER JOIN Sub_Category_Master ON Sub_Category_Master.MainCategoryId=Main_Category_Master.MainCategoryId ORDER BY MainCategoryName

	--Get Main Category Names
	SELECT MainCategoryId,MainCategoryName FROM Main_Category_Master

	--Get Sub Category Names
	CREATE PROCEDURE Get_SubCategory_Names(@MainCategoryId INT)
	AS BEGIN
	SELECT SubCategoryId,SubCategoryName FROM Sub_Category_Master WHERE MainCategoryId=@MainCategoryId
	END

	EXEC Get_SubCategory_Names 3

	
	SELECT * FROM Signup

	CREATE PROCEDURE Check_Logins(@Email VARCHAR(50),@Password VARCHAR(50))
	AS BEGIN 
	SELECT Email,Password FROM Signup WHERE Email=@Email AND Password=@Password
	END 

	EXEC Check_Logins 'raja@gmail.com' ,'1234'