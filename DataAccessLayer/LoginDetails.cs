﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class LoginDetails
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public string Active { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
