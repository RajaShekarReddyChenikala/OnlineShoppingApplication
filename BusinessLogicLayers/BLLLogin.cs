﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessLayers;
using BusinessObjectsLayer;

namespace BusinessLogicLayers
{
    public class BLLLogin
    {
        DALLogin objdal = new DALLogin();
        public string CheckCredentials(BOLLogin bollogin)
        {
           return objdal.CheckCredentials(bollogin);
        }
    }
}
