﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BusinessObjectsLayer;
using DataAccessLayers;

namespace BusinessLogicLayers
{
    public class BLLSubCategoryMaster
    {
        DALSubCategoryMaster dalsub = new DALSubCategoryMaster();
        public int PostSubCategoryDetails(BOLSubCategoryMaster bolsub)
        {
            int i = dalsub.PostSubCategoryDetails(bolsub);
            return i;
        }
        public DataTable GetMainCategoryName()
        {
            DataTable dt = dalsub.GetMainCategoryName();
            return dt;
        }
        public DataTable GetAllSubCategoryDetails()
        {
            DataTable dt = dalsub.GetAllSubCategoryDetails();
            return dt;
        }
        public int DeleteSubCategoryDetails(int id)
        {
            return dalsub.DeleteSubCategoryDetails(id);
        }
    }
    
}
