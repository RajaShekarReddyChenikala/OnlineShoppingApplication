﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessLayers;
using BusinessObjectsLayer;

namespace BusinessLogicLayers
{
    public class BLLSignup
    {
        DALSignup dalsignup = new DALSignup();
        public int PostSignupCredentials(BOLSignup bolsignup)
        {
            return dalsignup.PostSignupCredentials(bolsignup);
        }
    }
}
