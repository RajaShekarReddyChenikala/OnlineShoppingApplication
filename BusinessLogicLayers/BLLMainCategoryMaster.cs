﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessLayers;
using BusinessObjectsLayer;

namespace BusinessLogicLayers
{
    public class BLLMainCategoryMaster
    {
        DALMainCategoryMaster dalmcm = new DALMainCategoryMaster();
        public int PostMainCategoryDetails(BOLMainCategoryMaster bolmain)
        {
            int i=dalmcm.PostMainCategoryDetails(bolmain);
            return i;
        }
        public DataTable GetAllMainCategoryDetails()
        {
            DataTable dt = dalmcm.GetAllMainCategoryDetails();
            return dt;
        }
        public int DeleteMainCategoryDetails(int id)
        {
            return dalmcm.DeleteMainCategoryDetails(id);
        }
        public int UpdateMainCategoryDetails(int id, BOLMainCategoryMaster bolmain)
        {
            return dalmcm.UpdateMainCategoryDetails(id, bolmain);
        }
    }
}
