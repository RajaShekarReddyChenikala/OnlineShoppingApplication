﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BusinessObjectsLayer;
using DataAccessLayers;

namespace BusinessLogicLayers
{
    public class BLLProductMaster
    {
        DALProductMaster dalproduct = new DALProductMaster();
        public int PostProductMasterDetails(BOLProductMaster bolproduct)
        {
            int i = dalproduct.PostProductMasterDetails(bolproduct);
            return i;
        }
        public DataTable GetSubCategoryNames(int id)
        {
            DataTable dt = dalproduct.GetSubCategoryNames(id);
            return dt;
        }
        public DataTable GetAllProductMasterDetails()
        {
            DataTable dt = dalproduct.GetAllProductMasterDetails();
            return dt;
        }
        public int DeleteProductMasterDetails(int id)
        {
            return dalproduct.DeleteProductMasterDetails(id);
        }
    }
}
