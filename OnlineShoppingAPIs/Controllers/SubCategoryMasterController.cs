﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Web.Http;
using BusinessObjectsLayer;
using BusinessLogicLayers;

namespace OnlineShoppingAPIs.Controllers
{
    public class SubCategoryMasterController : ApiController
    {
        BLLSubCategoryMaster bllsub = new BLLSubCategoryMaster();
        //[Authorize]
        [HttpPost]
        public string PostSubCategoryDetails(BOLSubCategoryMaster bolsub)
        {
            int i = bllsub.PostSubCategoryDetails(bolsub);
            if (i == 1)
            {
                return "Sub Category Details Added";
            }
            else
            {
                return "Failed";
            }
        }
        [Route("api/SubCategoryMaster/GetMainCategoryDetails")]
        //[Authorize]
        [HttpGet]
        public DataTable GetMainCategoryDetails()
        {
            DataTable dt = bllsub.GetMainCategoryName();
            return dt;
        }
        [Route("api/SubCategoryMaster/GetAllSubCategoryDetails")]
        //[Authorize]
        [HttpGet]
        public DataTable GetAllSubCategoryDetails()
        {
            DataTable dt = bllsub.GetAllSubCategoryDetails();
            return dt;
        }
        //[Authorize]
        [HttpDelete]
        public string DeleteSubCategoryDetails(int id)
        {
            int i = bllsub.DeleteSubCategoryDetails(id);
            if (i == 1)
            {
                return "Deleted Sub Category Details";
            }
            else
            {
                return "Failed";
            }
        }
    }
}
