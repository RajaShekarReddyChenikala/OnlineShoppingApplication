﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using System.Web.Http;
using BusinessObjectsLayer;
using BusinessLogicLayers;

namespace OnlineShoppingAPIs.Controllers
{
    public class SignupController : ApiController
    {
        BLLSignup bllsignup = new BLLSignup();

        [HttpGet]
        public object GenerateToken()
        {
            string key = "Online_Shopping_Secret_Key_7170";
            var issuer = "http://localhost:56063";
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var getClaims = new List<Claim>();
            getClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            getClaims.Add(new Claim("Username", "Raja"));
            getClaims.Add(new Claim("Password", "Raja@321"));

            var token = new JwtSecurityToken(
                issuer,
                issuer,
                getClaims,
                expires: DateTime.Now.AddDays(5),
                signingCredentials: credentials
                );
            var newToken = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = newToken };
        }
        //[Authorize]
        [HttpPost]
        public HttpResponseMessage PostSignupCredentials(BOLSignup bolsignup)
        {
            try
            {
                bllsignup.PostSignupCredentials(bolsignup);
                return Request.CreateResponse(HttpStatusCode.Created, "User Details Saved Successfully");                
            }
            catch(Exception ex)
            {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ex);
            }
        }
    }
}
