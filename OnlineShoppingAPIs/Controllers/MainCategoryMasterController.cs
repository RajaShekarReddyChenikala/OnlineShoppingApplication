﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using BusinessObjectsLayer;
using BusinessLogicLayers;

namespace OnlineShoppingAPIs.Controllers
{
    public class MainCategoryMasterController : ApiController
    {
        BLLMainCategoryMaster bllmcm = new BLLMainCategoryMaster();
        //[Authorize]
        [HttpPost]
        public string PostMainCategoryDetails(BOLMainCategoryMaster bolmain)
        {
            int i = bllmcm.PostMainCategoryDetails(bolmain);
            if (i == 1)
            {
                return "Main Category Details Added";
            }
            else
            {
                return "Failed";
            }
        }
        //[Authorize]
        [HttpGet]
        public DataTable GetAllMainCategoryDetails()
        {
            DataTable dt = bllmcm.GetAllMainCategoryDetails();
            return dt;
        }
       // [Authorize]
        [HttpDelete]
        public string DeleteMainCategoryDetails(int id)
        {
            int i= bllmcm.DeleteMainCategoryDetails(id);
            if (i == 1)
            {
                return "Deleted Main Category Details";
            }
            else
            {
                return "Failed";
            }
        }
        [HttpPut]
        public string UpdateMainCategoryDetails(int id, BOLMainCategoryMaster bolmain)
        {
            int i= bllmcm.UpdateMainCategoryDetails(id, bolmain);
            if (i == 1)
            {
                return "Updated Main Category Details";
            }
            else
            {
                return "Failed";
            }
        }
    }
}
