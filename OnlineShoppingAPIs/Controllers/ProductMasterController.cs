﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using BusinessObjectsLayer;
using BusinessLogicLayers;

namespace OnlineShoppingAPIs.Controllers
{
    public class ProductMasterController : ApiController
    {
        BLLProductMaster bllproduct = new BLLProductMaster();
        //[Authorize]
        [HttpPost]
        public string PostProductMasterDetails(BOLProductMaster bolproduct)
        {
            int i = bllproduct.PostProductMasterDetails(bolproduct);
            if (i == 1)
            {
                return "Product Master Details Added";
            }
            else
            {
                return "Failed";
            }
        }
        [Route("api/ProductMaster/GetSubCategoryNames")]
        //[Authorize]
        [HttpGet]
        public DataTable GetSubCategoryNames(int id)
        {
            DataTable dt = bllproduct.GetSubCategoryNames(id);
            return dt;
        }
        [Route("api/ProductMaster/GetAllProductMasterDetails")]
        //[Authorize]
        [HttpGet]
        public DataTable GetAllProductMasterDetails()
        {
            DataTable dt = bllproduct.GetAllProductMasterDetails();
            return dt;
        }
        //[Authorize]
        [HttpDelete]
        public string DeleteProductMasterDetails(int id)
        {
            int i = bllproduct.DeleteProductMasterDetails(id);
            if (i == 1)
            {
                return "Deleted Product Master Details";
            }
            else
            {
                return "Failed";
            }
        }
    }
}
