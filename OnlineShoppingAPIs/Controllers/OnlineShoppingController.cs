﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Web.Http;
using BusinessObjectsLayer;
using BusinessLogicLayers;

namespace OnlineShoppingAPIs.Controllers
{
    public class OnlineShoppingController : ApiController
    {
        BLLLogin objbll = new BLLLogin();
        [Route("api/OnlineShopping/CheckCredentials")]
        //[Authorize]
        [HttpPost]
        public HttpResponseMessage CheckCredentials(BOLLogin bollogin)
        {
            try
            {
                objbll.CheckCredentials(bollogin);
                return Request.CreateResponse(HttpStatusCode.OK,"Matched");
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ex);
            }
        }

    }
}
