﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPage.Master" AutoEventWireup="true" CodeBehind="MainCategory.aspx.cs" Inherits="ASPOnlineShoppingApplication.MainCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 362px;
        }
        .auto-style3 {
            width: 347px;
        }
        .auto-style4 {
            width: 1285px;
        }
        .auto-style5 {
            width: 170px;
        }
        .auto-style6 {
            width: 221px;
        }
        h2,h4{
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Welcome to main category page
    </p>
    <table class="auto-style4">
        <h2>Main Category Details</h2>
        <p>&nbsp;</p>
        <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style5">
                Enter Main Category
            </td>
            <td class="auto-style6">
                <asp:TextBox ID="maincategory" runat="server" Height="24px" Width="190px"></asp:TextBox>
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style3">
                &nbsp;</td>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style5">Status</td>
            <td class="auto-style6">
                <asp:CheckBox ID="statuscheck" runat="server" />
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style5">
            </td>
            <td class="auto-style6">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="save" runat="server" Text="SAVE" OnClick="save_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="cancel" runat="server" Text="CANCEL" OnClick="cancel_Click" />
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
    </table>
    <h4>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </h4>
</asp:Content>
