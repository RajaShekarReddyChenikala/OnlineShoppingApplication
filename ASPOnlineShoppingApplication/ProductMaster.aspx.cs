﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace ASPOnlineShoppingApplication
{
    public partial class ProductMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var MainCategoryId = maindropdown.SelectedIndex;
            var SubCategoryId = subdropdown.SelectedIndex;
            var ProductName = productname.Text;
            var Status = productcheck.Checked;

            var ProductDetails = new
            {
                ProductName = ProductName,
                Active = Status,
                MainCategoryId = MainCategoryId,
                SubCategoryId = SubCategoryId
            };
            string apiUrl = "http://localhost:56063/api/ProductMaster/PostProductMasterDetails";
            string inputJson = new JavaScriptSerializer().Serialize(ProductDetails);
            HttpClient client = new HttpClient();
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Label1.Text = "Product Details Inserted Successfully";
            }
            else
            {
                Label1.Text = "Failed";
            }
            maindropdown.SelectedIndex = 0;
            subdropdown.SelectedIndex = 0;
            productname.Text = "";
            productcheck.Checked = false;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            maindropdown.SelectedIndex = 0;
            subdropdown.SelectedIndex = 0;
            productname.Text = "";
            productcheck.Checked = false;
        }
    }
}