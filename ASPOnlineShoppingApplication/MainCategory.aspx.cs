﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace ASPOnlineShoppingApplication
{
    public partial class MainCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void save_Click(object sender, EventArgs e)
        {
            var MainCategory = maincategory.Text;
            var Status = statuscheck.Checked;

            var MainCategoryDetails = new
            {
                MainCategoryName = MainCategory,
                Active = Status
            };
            string apiUrl = "http://localhost:56063/api/MainCategoryMaster/PostMainCategoryDetails";
            string inputJson = new JavaScriptSerializer().Serialize(MainCategoryDetails);
            HttpClient client = new HttpClient();
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Label1.Text = "Main Category Details Inserted Successfully";
            }
            else
            {
                Label1.Text = "Failed";
            }
            maincategory.Text = "";
            statuscheck.Checked =false;
        }
        protected void cancel_Click(object sender, EventArgs e)
        {
            maincategory.Text = "";
            statuscheck.Checked =false;
        }
    }
}
