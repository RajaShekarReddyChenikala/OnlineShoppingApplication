﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace ASPOnlineShoppingApplication
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            var UserName = username.Text;
            var Password = password.Text;

            var Credentials = new
            {
                Email = UserName,
                Password = Password
            };
            string apiUrl = "http://localhost:56063/api/OnlineShopping/CheckCredentials";
            string inputJson = new JavaScriptSerializer().Serialize(Credentials);
            HttpClient client = new HttpClient();
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Response.Redirect("MainCategory.aspx");
                Label1.Text = "User Credentials Matched successfully";             
            }
            else
            {
                Label1.Text = "Login Failed";
            }
            ClearLoginForm();
        }
        protected void ClearLoginForm()
        {
            username.Text = "";
            password.Text = "";
        }
    }
}