﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace ASPOnlineShoppingApplication
{
    public partial class SubCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var MainCategoryId = maindropdown.SelectedIndex;
            var SubCategory = subcategory.Text;
            var Status = substatus.Checked;

            var SubCategoryDetails = new
            {
                SubCategoryName = SubCategory,
                Active = Status,
                MainCategoryId= MainCategoryId
            };
            string apiUrl = "http://localhost:56063/api/SubCategoryMaster/PostSubCategoryDetails";
            string inputJson = new JavaScriptSerializer().Serialize(SubCategoryDetails);
            HttpClient client = new HttpClient();
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Label1.Text = "Sub Category Details Inserted Successfully";
            }
            else
            {
                Label1.Text = "Failed";
            }
            maindropdown.SelectedValue = "";
            subcategory.Text = "";
            substatus.Checked = false;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            maindropdown.SelectedIndex =0;
            subcategory.Text = "";
            substatus.Checked = false;
        }
    }
}