﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnlineShopping.Master" AutoEventWireup="true" CodeBehind="Signup.aspx.cs" Inherits="ASPOnlineShoppingApplication.Signup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style type="text/css">
        h2,h4,h5{
            text-align:center;
        }
        .auto-style6 {
            width: 636px;
        }
        .auto-style8 {
            width: 154px;
        }
        .auto-style9 {
            width: 483px;
        }
    .auto-style10 {
        width: 161px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Welcome To Signup Page...!
    </p>
    <div>
    <table>
       <h2>SignUp</h2>
        <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    Name
            </td>
            <td class="auto-style10">
                <asp:TextBox ID="username" runat="server" Width="190px" Height="24px"></asp:TextBox>
            </td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    &nbsp;</td>
            <td class="auto-style10">
                &nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    Email
            </td>
            <td class="auto-style10">
                <asp:TextBox ID="email" runat="server" Width="190px" Height="24px"></asp:TextBox>
            </td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    &nbsp;</td>
            <td class="auto-style10">
                &nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    Password
            </td>
            <td class="auto-style10">
                <asp:TextBox ID="password" runat="server" Width="190px" Height="24px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    &nbsp;</td>
            <td class="auto-style10">
                &nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    Confirm Password</td>
            <td class="auto-style10">
                <asp:TextBox ID="confirmpwd" runat="server" Height="24px" TextMode="Password" Width="190px"></asp:TextBox>
             </td>
            <td class="auto-style6">
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="password" ControlToValidate="confirmpwd" ErrorMessage="Pwd and Cpwd both must be same " ForeColor="Red"></asp:CompareValidator>
             </td>
        </tr>
         <tr>
            <td class="auto-style9">
                    &nbsp;</td>
            <td class="auto-style8">
                    &nbsp;</td>
            <td class="auto-style10">
                &nbsp;</td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style9">
                    
                &nbsp;</td>
            <td class="auto-style8">
                    
            </td>
            <td class="auto-style10">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="SignUp" OnClick="Button1_Click"/>
            </td>
            <td class="auto-style6">
                &nbsp;</td>
        </tr>
    </table>
        <h5><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h5>
    <br />
    <h4>Already Have An  Account? Please  <a href="Login.aspx" Text="Login" Value="Login"><strong>Login</strong></a></h4>
        </div>
    <script type="text/javascript">
        //function SignUp() {
        //    let userName = $("#username").val();
        //    let email = $("#email").val();
        //    let password = $("#password").val();


        //    var data = JSON.stringify({
        //        "UserName": userName,
        //        "Email": email,
        //        "Password": password

        //    });
        //    $.ajax({
        //        url: 'http://localhost:56063/api/Signup/PostSignupCredentials',
        //        type: 'POST',
        //        headers: {
        //            Authorization: `Bearer ${token}`
        //        },
        //        data: data,
        //        dataType: 'json',
        //        contentType: 'application/json; charset=utf-8',
        //        success: function (response) {
        //            alert(response);
        //        },
        //        error: function (error) {
        //            alert(error);
        //        }
        //    });
        //    clearSignUps();
        //}
        //function clearSignUps() {
        //    $("#username").val('');
        //    $("#email").val('');
        //    $("#password").val('');
        //}
    </script>
</asp:Content>
