﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminPage.Master" AutoEventWireup="true" CodeBehind="SubCategory.aspx.cs" Inherits="ASPOnlineShoppingApplication.SubCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Welcome to sub Category page
    </p>
    <table>
        <h2>Sub Categories</h2>
        <p>&nbsp;</p>
        <tr>
            <td>
                Select Main Category
            </td>
            <td>
                <asp:DropDownList ID="maindropdown" runat="server" Height="19px">
                    <asp:ListItem>Please Select</asp:ListItem>
                    <asp:ListItem>Bikes</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Enter Sub Category 
            </td>
            <td>
                <asp:TextBox ID="subcategory" runat="server" Height="24px" Width="190px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Status
            </td>
            <td>
                <asp:CheckBox ID="substatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="SAVE" OnClick="Button1_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="CANCEL" OnClick="Button2_Click" />
            </td>
        </tr>
    </table>
    <h4>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </h4>
</asp:Content>
