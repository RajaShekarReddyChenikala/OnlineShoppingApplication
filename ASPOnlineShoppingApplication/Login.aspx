﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnlineShopping.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ASPOnlineShoppingApplication.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
        width: 485px;
    }
        .auto-style4 {
            width: 130px;
        }
        h2,h4,h5{
            text-align:center
        }
    .auto-style5 {
        width: 199px;
    }
    .auto-style6 {
        width: 455px;
    }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <h2>Login Form</h2>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style4">
                UserName
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="username" runat="server" Width="190px" Height="24px"></asp:TextBox>
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style4">
                &nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style4">
                Password
            </td>
            <td class="auto-style5">
                <asp:TextBox ID="password" runat="server" Width="190px" Height="24px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style4">
                &nbsp;</td>
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style4">
            </td>
            <td class="auto-style5">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
    </table>
    <h5><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h5>
    <br />
    <h4>Don't Have An Account? Please  <a href="Signup.aspx" Text="Signup" Value="Signup"><strong>Signup</strong></a></h4>
</asp:Content>
