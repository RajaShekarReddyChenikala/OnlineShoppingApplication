﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace ASPOnlineShoppingApplication
{
    public partial class Signup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var Name = username.Text;
            var Email = email.Text;
            var Password = password.Text;
            var ConfirmPassword = confirmpwd.Text;

            var UserDatails = new
            {
                UserName=Name,
                Email=Email,
                Password=Password,
                ConfirmPassword=ConfirmPassword
            };
            string apiUrl = "http://localhost:56063/api/Signup/PostSignupCredentials";
            string inputJson = new JavaScriptSerializer().Serialize(UserDatails);
            HttpClient client = new HttpClient();
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Label1.Text="User Data Inserted Successfully";
            }
            else
            {
                Label1.Text = "Failed";
            }
            ClearSignupForm();
        }
        protected void ClearSignupForm()
        {
            username.Text = "";
            email.Text = "";
            password.Text = "";
            confirmpwd.Text = "";

        }
    }
}