﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class LoginBLL:ILogin
    {
        string connection = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        public string PostLoginCredenials(LoginDetails login)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connection);
            SqlCommand cmd = new SqlCommand("Post_Logins", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", login.UserName);
            cmd.Parameters.AddWithValue("@Password", login.Password);
            cmd.Parameters.AddWithValue("@Active", login.Active);
            cmd.Parameters.AddWithValue("@LastLogin", login.LastLogin);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return "Login Credentials Saved Successfully";
        }
    }
}
